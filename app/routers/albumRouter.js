const express = require('express');

const router = express.Router();

const infoMiddleware = require('../middleWare/infoMiddleware');

const {createAlbum, getAllAlbum, getAlbumById, updateAlbumById, deleteAlbumById, getAlbumOfUser} = require('../controller/albumController');

router.use(infoMiddleware)

router.get('/albums', getAllAlbum);
router.post('/albums', createAlbum);
router.get('/albums/:albumId', getAlbumById);
router.get('/users/:userId/albums', getAlbumOfUser);

router.put('/albums/:albumId', updateAlbumById);
router.delete('/albums/:albumId', deleteAlbumById);

module.exports = router