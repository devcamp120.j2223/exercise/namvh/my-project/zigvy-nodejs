const express = require('express');

const router = express.Router();

const infoMiddleware = require('../middleWare/infoMiddleware');

const {createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById, getTodoOfUser} = require('../controller/todoController');

router.use(infoMiddleware)

router.get('/todos', getAllTodo);
router.post('/todos', createTodo);
router.get('/todos/:todoId', getTodoById);
router.get('/users/:userId/todos', getTodoOfUser);

router.put('/todos/:todoId', updateTodoById);
router.delete('/todos/:todoId', deleteTodoById);

module.exports = router