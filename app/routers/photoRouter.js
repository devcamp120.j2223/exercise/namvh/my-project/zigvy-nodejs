const express = require('express');

const router = express.Router();

const infoMiddleware = require('../middleWare/infoMiddleware');

const {createPhoto, getAllPhoto, getPhotoById, updatePhotoById, deletePhotoById, getPhotoOfAlbum} = require('../controller/photoController');

router.use(infoMiddleware)

router.get('/photos', getAllPhoto);
router.post('/photos', createPhoto);
router.get('/photos/:photoId', getPhotoById);
router.get('/albums/:albumId/photos', getPhotoOfAlbum);

router.put('/photos/:photoId', updatePhotoById);
router.delete('/photos/:photoId', deletePhotoById);

module.exports = router