const todoModel = require('../models/todoModel');

const mongoose = require("mongoose");
const { use } = require('../routers/postRouter');

const createTodo = (req, res) => {
    //B1
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is required"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
   
    //B3
    const bodyInfo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyReq.userId,
        title: bodyReq.title,
        completed: bodyReq.completed
    }

    todoModel.create(bodyInfo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create todo success!!!",
                data: data
            })
        }
    }) 
}

const getAllTodo = (req, res) => {
    //B1
    let userId = req.query.userId;

    let condition = {};

    if(userId){
        condition.userId = userId;
    }
    //B3
    todoModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Todo success!!!",
                data: data
            })
        }
    })
}

const getTodoById = (req, res) => {
    //B1
    let todoId = req.params.todoId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "todoId is invalid"
        })
    }
    //B3
    todoModel.findById(todoId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Todo By Id success!!!",
                data: data
            })
        }
    })
}

const updateTodoById = (req, res) => {
    //B1
    let todoId = req.params.todoId;
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "todoId in invalid"
        })
    }
  
    //B3
    let bodyInfo = {
        userId: bodyReq.userId,
        title: bodyReq.title,
        completed: bodyReq.completed
    }
    todoModel.findByIdAndUpdate(todoId, bodyInfo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Todo success!!!",
                data: data
            })
        }
    })
}

const deleteTodoById = (req, res) => {
    //B1:
    let todoId = req.params.todoId; 
    //B2
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "todoId is invalid"
        })
    }
    //B3
    todoModel.findByIdAndDelete(todoId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete Todo success"
            })
        }
    })
}


const getTodoOfUser = (req, res) => {
    //B1
    let userId = req.params.userId;

    let condition = {};
    //B2
    if(userId){
        condition.userId = userId;
    }
    //B3
    todoModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Todo success!!!",
                data: data
            })
        }
    })
}

module.exports = {createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById, getTodoOfUser}