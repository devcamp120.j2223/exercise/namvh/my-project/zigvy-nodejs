const photoModel = require('../models/photoModel');

const mongoose = require("mongoose");
const { use } = require('../routers/postRouter');

const createPhoto = (req, res) => {
    //B1
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is required"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if(!bodyReq.url) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "url is required"
        })
    }
    if(!bodyReq.thumbnailUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "thumbnailUrl is required"
        })
    }
   
    //B3
    const bodyInfo = {
        _id: mongoose.Types.ObjectId(),
        albumId: bodyReq.albumId,
        title: bodyReq.title,
        url: bodyReq.url,
        thumbnailUrl: bodyReq.thumbnailUrl
    }

    photoModel.create(bodyInfo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create photo success!!!",
                data: data
            })
        }
    }) 
}

const getAllPhoto = (req, res) => {
    //B1
    let albumId = req.query.albumId;

    let condition = {};

    if(albumId){
        condition.albumId = albumId;
    }
    //B3
    photoModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Photo success!!!",
                data: data
            })
        }
    })
}

const getPhotoById = (req, res) => {
    //B1
    let photoId = req.params.photoId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "photoId is invalid"
        })
    }
    //B3
    photoModel.findById(photoId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Photo By Id success!!!",
                data: data
            })
        }
    })
}

const updatePhotoById = (req, res) => {
    //B1
    let photoId = req.params.photoId;
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "photoId in invalid"
        })
    }
  
    //B3
    let bodyInfo = {
        photoId: bodyReq.photoId,
        title: bodyReq.title,
        url: bodyReq.url,
        thumbnailUrl: bodyReq.thumbnailUrl
    }
    photoModel.findByIdAndUpdate(photoId, bodyInfo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Photo success!!!",
                data: data
            })
        }
    })
}

const deletePhotoById = (req, res) => {
    //B1:
    let photoId = req.params.photoId; 
    //B2
    if(!mongoose.Types.ObjectId.isValid(photoId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "photoId is invalid"
        })
    }
    //B3
    photoModel.findByIdAndDelete(photoId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete Photo success"
            })
        }
    })
}


const getPhotoOfAlbum = (req, res) => {
    //B1
    let albumId = req.params.albumId;

    let condition = {};
    //B2
    if(albumId){
        condition.albumId = albumId;
    }
    //B3
    photoModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Photo success!!!",
                data: data
            })
        }
    })
}

module.exports = {createPhoto, getAllPhoto, getPhotoById, updatePhotoById, deletePhotoById, getPhotoOfAlbum}