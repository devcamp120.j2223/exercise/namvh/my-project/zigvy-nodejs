const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 8000;

mongoose.connect('mongodb://localhost:27017/Zigvy_Technical', (err) => {
    if(err){
        throw err;
    }
    console.log("Connect mongoose successfully !!!");
})

const userRouter = require('./app/routers/userRouter');
const postRouter = require('./app/routers/postRouter');
const commentRouter = require('./app/routers/commentRouter');
const albumRouter = require('./app/routers/albumRouter');
const photoRouter = require('./app/routers/photoRouter');
const todoRouter = require('./app/routers/todoRouter');

app.use(express.json());

app.use(express.urlencoded({
    extended: true
}))

app.use('/', userRouter);
app.use('/', postRouter);
app.use('/', commentRouter);
app.use('/', albumRouter);
app.use('/', photoRouter);
app.use('/', todoRouter);



app.listen(port, () => {
    console.log(`App is running success !!! ${port} `);
})