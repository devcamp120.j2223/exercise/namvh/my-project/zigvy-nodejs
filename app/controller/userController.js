const userModel = require('../models/userModel');

const mongoose = require("mongoose");

const createUser = (req, res) => {
    //B1
    let bodyReq = req.body;
    //B2
    if(!bodyReq.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!bodyReq.username) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userName is required"
        })
    }
    if(!bodyReq.address) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "address is required"
        })
    }
    if(!bodyReq.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if(!bodyReq.website) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "website is required"
        })
    }
    if(!bodyReq.company) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "company is required"
        })
    }
    //B3
    const bodyInfo = {
        _id: mongoose.Types.ObjectId(),
        name: bodyReq.name,
        username: bodyReq.username,
        address: bodyReq.address,
        phone: bodyReq.phone,
        website: bodyReq.website,
        company: bodyReq.company
    }

    userModel.create(bodyInfo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create user success!!!",
                data: data
            })
        }
    }) 
}


const getAllUser = (req, res) => {
    //B1
    let userId = req.query.userId; 
    let condition = {};
    if(userId){
        condition.userId = userId;
    }
    //B3
    userModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Get All User success!!!",
                data: data
            })
        }
    })
}

const getAllUserById = (req, res) => {
    //B1
    let userId = req.params.userId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "UserId is invalid"
        })
    }
    //B3
    userModel.findById(userId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Get All User success!!!",
                data: data
            })
        }
    })
}

const updateUserById = (req, res) => {
    //B1
    let userId = req.params.userId;
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "UserId in invalid"
        })
    }
    
    //B3
    let bodyInfo = {
        name: bodyReq.name,
        username: bodyReq.username,
        address: bodyReq.address,
        phone: bodyReq.phone,
        website: bodyReq.website,
        company: bodyReq.company
    }
    userModel.findByIdAndUpdate( userId, bodyInfo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update User success!!!",
                data: data
            })
        }
    })
}

const deleteUserById = (req, res) => {
    //B1
    let userId = req.params.userId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: err.message
        })
    }
    //B3
    userModel.findByIdAndDelete(userId, (err) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete user successfully!!!"
            })
        }
    })
}


const getPostOfUser = (req, res) => {
    //B1
    let userId = req.params.userId;
    let condition =  {};

    if(userId){
        condition.userId = userId;
    }
    //B3
    userModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Get All User success!!!",
                data: data
            })
        }
    })
}

module.exports = {createUser, getAllUser, getAllUserById, updateUserById, deleteUserById, getPostOfUser}