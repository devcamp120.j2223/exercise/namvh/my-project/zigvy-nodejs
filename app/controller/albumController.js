const albumModel = require('../models/albumModel');

const mongoose = require("mongoose");
const { use } = require('../routers/postRouter');

const createAlbum = (req, res) => {
    //B1
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is required"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
   
    //B3
    const bodyInfo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyReq.userId,
        title: bodyReq.title,
       
    }

    albumModel.create(bodyInfo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create album success!!!",
                data: data
            })
        }
    }) 
}

const getAllAlbum = (req, res) => {
    //B1
    let userId = req.query.userId;

    let condition = {};

    if(userId){
        condition.userId = userId;
    }
    //B3
    albumModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Album success!!!",
                data: data
            })
        }
    })
}

const getAlbumById = (req, res) => {
    //B1
    let albumId = req.params.albumId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    //B3
    albumModel.findById(albumId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Album By Id success!!!",
                data: data
            })
        }
    })
}

const updateAlbumById = (req, res) => {
    //B1
    let albumId = req.params.albumId;
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId in invalid"
        })
    }
  
    //B3
    let bodyInfo = {
        userId: bodyReq.userId,
        title: bodyReq.title,
    }
    albumModel.findByIdAndUpdate(albumId, bodyInfo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Album success!!!",
                data: data
            })
        }
    })
}

const deleteAlbumById = (req, res) => {
    //B1:
    let albumId = req.params.albumId; 
    //B2
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "albumId is invalid"
        })
    }
    //B3
    albumModel.findByIdAndDelete(albumId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete Album success"
            })
        }
    })
}


const getAlbumOfUser = (req, res) => {
    //B1
   let userId = req.params.userId

    let condition = {};
    //B2
    if(userId){
        condition.userId = userId;
    }
    //B3
    albumModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Album success!!!",
                data: data
            })
        }
    })
}

module.exports = {createAlbum, getAllAlbum, getAlbumById, updateAlbumById, deleteAlbumById, getAlbumOfUser}