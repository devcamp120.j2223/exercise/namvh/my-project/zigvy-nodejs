const express = require('express');

const router = express.Router();

const infoMiddleware = require('../middleWare/infoMiddleware');

const {createComment, getAllComment, getAllCommentById, updateCommentById, deleteCommentById, getCommentOfPost} = require('../controller/commentController');

router.use(infoMiddleware)

router.get('/comments', getAllComment);
router.post('/comments', createComment);
router.get('/comments/:commentId', getAllCommentById);
router.get('/posts/:postId/comments', getCommentOfPost);

router.put('/comments/:commentId', updateCommentById);
router.delete('/comments/:commentId', deleteCommentById);

module.exports = router