const infoMiddleware = (req, res, next) => {
    console.log(`Method: ${req.method} - URL: ${req.url} - Time: ${new Date()}`);
    next();
}

module.exports = infoMiddleware;