const express = require('express');

const router = express.Router();

const infoMiddleware = require('../middleWare/infoMiddleware');

const {createPost, getAllPost, getAllPostById, updatePostById, deletePostById, getPostOfUser} = require('../controller/postController');

router.use(infoMiddleware)

router.get('/posts', getAllPost);
router.post('/posts', createPost);
router.get('/posts/:postId', getAllPostById);
router.get('/users/:userId/posts', getPostOfUser);

router.put('/posts/:postId', updatePostById);
router.delete('/posts/:postId', deletePostById);

module.exports = router