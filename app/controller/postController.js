const postModel = require('../models/postModel');

const mongoose = require("mongoose");
const { use } = require('../routers/postRouter');

const createPost = (req, res) => {
    //B1
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is required"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if(!bodyReq.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    
    //B3
    const bodyInfo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyReq.userId,
        title: bodyReq.title,
        body: bodyReq.body,
    }

    postModel.create(bodyInfo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create post success!!!",
                data: data
            })
        }
    }) 
}

const getAllPost = (req, res) => {
    //B1
    let userId = req.query.userId;

    let condition = {};

    if(userId){
        condition.userId = userId;
    }
    //B3
    postModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Post success!!!",
                data: data
            })
        }
    })
}

const getAllPostById = (req, res) => {
    //B1
    let postId = req.params.postId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    //B3
    postModel.findById(postId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Post By Id success!!!",
                data: data
            })
        }
    })
}

const updatePostById = (req, res) => {
    //B1
    let postId = req.params.postId;
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(postId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId in invalid"
        })
    }
  
    //B3
    let bodyInfo = {
        userId: bodyReq.userId,
        title: bodyReq.title,
        body: bodyReq.body,
    }
    postModel.findByIdAndUpdate(postId, bodyInfo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Post success!!!",
                data: data
            })
        }
    })
}

const deletePostById = (req, res) => {
    //B1:
    let postId = req.params.postId; 
    //B2
    if(!mongoose.Types.ObjectId.isValid(postId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "PostId is invalid"
        })
    }
    //B3
    postModel.findByIdAndDelete(postId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete post success"
            })
        }
    })
}


const getPostOfUser = (req, res) => {
    //B1
    let userId = req.params.userId;

    let condition = {};
    //B2
    if(userId){
        condition.userId = userId;
    }
    //B3
    postModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Post success!!!",
                data: data
            })
        }
    })
}

module.exports = {createPost, getAllPost, getAllPostById, updatePostById, deletePostById, getPostOfUser}