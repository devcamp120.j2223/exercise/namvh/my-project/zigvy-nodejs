const express = require('express');

const router = express.Router();

const infoMiddleware = require('../middleWare/infoMiddleware');

const {createUser, getAllUser, getAllUserById, updateUserById, deleteUserById} = require('../controller/userController');

router.use(infoMiddleware)

router.get('/users', getAllUser);
router.post('/users', createUser);
router.get('/users/:userId', getAllUserById);
router.put('/users/:userId', updateUserById);
router.delete('/users/:userId', deleteUserById);

module.exports = router