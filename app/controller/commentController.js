const commentModel = require('../models/commentModel');

const mongoose = require("mongoose");
const { use } = require('../routers/postRouter');

const createComment = (req, res) => {
    //B1
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is required"
        })
    }
    if(!bodyReq.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!bodyReq.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if(!bodyReq.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    
    //B3
    const bodyInfo = {
        _id: mongoose.Types.ObjectId(),
        postId: bodyReq.postId,
        name: bodyReq.name,
        email: bodyReq.email,
        body: bodyReq.body,
    }

    commentModel.create(bodyInfo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Create comment success!!!",
                data: data
            })
        }
    }) 
}

const getAllComment = (req, res) => {
    //B1
    let postId = req.query.postId;

    let condition = {};

    if(postId){
        condition.postId = postId;
    }
    //B3
    commentModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Comment success!!!",
                data: data
            })
        }
    })
}

const getAllCommentById = (req, res) => {
    //B1
    let commentId = req.params.commentId;
    //B2
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId is invalid"
        })
    }
    //B3
    commentModel.findById(commentId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Comment By Id success!!!",
                data: data
            })
        }
    })
}

const updateCommentById = (req, res) => {
    //B1
    let commentId = req.params.commentId;
    let bodyReq = req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId in invalid"
        })
    }
  
    //B3
    let bodyInfo = {
        postId: bodyReq.postId,
        name: bodyReq.name,
        email: bodyReq.email,
        body: bodyReq.body,
    }
    commentModel.findByIdAndUpdate(commentId, bodyInfo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Comment success!!!",
                data: data
            })
        }
    })
}

const deleteCommentById = (req, res) => {
    //B1:
    let commentId = req.params.commentId; 
    //B2
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "commentId is invalid"
        })
    }
    //B3
    commentModel.findByIdAndDelete(commentId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Delete Comment success"
            })
        }
    })
}


const getCommentOfPost = (req, res) => {
    //B1
    let postId = req.params.postId;

    let condition = {};
    //B2
    if(postId){
        condition.postId = postId;
    }
    //B3
    commentModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Comment success!!!",
                data: data
            })
        }
    })
}

module.exports = {createComment, getAllComment, getAllCommentById, updateCommentById, deleteCommentById, getCommentOfPost}